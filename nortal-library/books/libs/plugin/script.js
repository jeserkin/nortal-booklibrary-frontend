var nortalBookLibrary = angular.module('nortalBookLibrary', ['ngRoute', 'ngSanitize']).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider)
{
	$routeProvider
		.when('/', {
			templateUrl: 'templates/list.html',
			controller:  'mainController'
		})
		.when('/:bookId', {
			templateUrl: 'templates/view.html',
			controller: 'bookController',
			resolve: {
				delay: function($q, $timeout)
				{
					var delay = $q.defer();
					$timeout(delay.resolve, 1000);
					return delay.promise;
				}
			}
		})
		.otherwise({
			redirectTo: '/'
		});

	$locationProvider.html5Mode(true);
}]);

nortalBookLibrary
	.filter('startFrom', function()
	{
		return function(input, start)
		{
			start = +start;
			return input.slice(start);
		};
	})
	.filter('range', function()
	{
		return function(input, total)
		{
			total = parseInt(total);

			for (var i=1; i<=total; i++)
			{
				input.push(i);
			}

			return input;
		};
	});

nortalBookLibrary
	.directive('bookRating', function()
	{
		return {
			restrict: 'E',
			templateUrl: 'templates/book-rating.html',
			scope: {
				value: '=',
				maxValue: '='
			},
			link: function(scope, element, attr)
			{
				scope.$watch('value', function()
				{
					scope.calcualted = scope.maxValue - scope.value;
				});
			}
		};
	});

nortalBookLibrary
	.run(function($rootScope, $location)
	{
		$rootScope.go = function(path)
		{
			$location.path(path);
		};

		$rootScope.isEmpty = function(obj)
		{
			return angular.equals({},obj);
		};
	});

nortalBookLibrary
	.controller('mainController', function($scope, $http)
	{
		$scope.orderByField = 'title';
		$scope.reverseSort  = false;

		$http({method: 'GET', url: 'http://localhost:8080/nortal-library/api/books'})
			.success(function(data, status, headers, config)
			{
				for (var key in data)
				{
					data[key].authors   = data[key].authors.join("<br />");
					data[key].tags      = data[key].tags.join("; ");
					data[key].available = data[key].available > 0;
				}

				//testNotExistentBook(data); // For test purpose
				$scope.books = data;
			})
			.error(function(data, status, headers, config)
			{
				$scope.books = [];
			});

		$scope.booksPageManager = new BooksPageManager($scope.books);
	});

nortalBookLibrary
	.controller('bookController', function($scope, $http, $routeParams)
	{
		$http({method: 'GET', url: 'http://localhost:8080/nortal-library/api/books/' + $routeParams.bookId})
			.success(function(data, status, headers, config)
			{
				for (var key in data.borrowers)
				{
					data.borrowers[key].from = new Date(data.borrowers[key].from);
					data.borrowers[key].to = new Date(data.borrowers[key].to);
				}

				$scope.book  = data;
				$scope.error = {};
			})
			.error(function(data, status, headers, config)
			{
				$scope.book = {};
				$scope.error = {
					status: status,
					message: data
				};
			});
	});

function BooksPageManager(list, pageSize)
{
	this.currentPage = 0;
	this.pageSize    = pageSize || 5;
	this.list        = list;
}

BooksPageManager.prototype.numberOfPages = function()
{
	return Math.ceil(this.list.length/this.pageSize);
};

BooksPageManager.prototype.getCurrentPage = function()
{
	return this.currentPage;
};

BooksPageManager.prototype.setCurrentPage = function(page)
{
	if (page <= 0)
	{
		this.currentPage = 0;
	}
	else if (page >= this.numberOfPages())
	{
		this.currentPage = this.numberOfPages() - 1;
	}
	else
	{
		this.currentPage = page;
	}
};

BooksPageManager.prototype.getPageSize = function()
{
	return this.pageSize;
};

BooksPageManager.prototype.getList = function()
{
	return this.list;
};

/* Test missing book */
function testNotExistentBook(books)
{
	var testBook = {};
	angular.copy(books[books.length - 1], testBook);

	testBook.id       = books.length + 1;
	testBook.location = "Harjumaa";
	books.push(testBook);

	return books;
}